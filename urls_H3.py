url_gopro_on = "/bacpac/PW?t={0}&p=%01"
url_gopro_off = "/bacpac/PW?t={0}&p=%00"

url_shutter_on = "/bacpac/SH?t={0}&p=%01"
url_shutter_off = "/bacpac/SH?t={0}&p=%00"

url_mode_video = "/camera/CM?t={0}&p=%00"
url_mode_photo = "/camera/CM?t={0}&p=%01"
url_mode_burst = "/camera/CM?t={0}&p=%01"

url_fov_wide = "/camera/FV?t={0}&p=%00"
url_fov_med = "/camera/FV?t={0}&p=%01"
url_fov_narrow = "/camera/FV?t={0}&p=%02"
url_vol_no = "/camera/BS?t={0}&p=%00"
url_del_last = "/camera/DL?t={0}&p=%00"
url_led_no = "/camera/LB?t={0}&p=%00"
url_autooff_no = "/camera/AO?t={0}&p=%00"
url_photres_7M = "/camera/PR?t={0}&p=%04"
url_delete_last = "/camera/DL?t={0}"
url_delete_all = "/camera/DA?t={0}"

url_set_date_time = "/camera/TM?t={0}&p=%"
